﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
           
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                if (char.IsLetter(source[i]) || source[i] == '.' || source[i] == ',')
                {
                    string message = "You entered the letters";
                    throw new ArgumentException(message);
                }
            }
            int value;
            if (Convert.ToDouble(source) % 1 == 0)
            {
                value = Convert.ToInt32(source);
            }
            else throw new NotImplementedException();
            return value;
        }

        public int Multiplication(int num1, int num2)
        {
            int mult = 0;
            for (int i = 0; i < Math.Abs(num2); i++)
            {
                mult = mult + num1;
            }
            if (num2 < 0 && num1 > 0) mult *= -1;
            if (num2 < 0 && num1 < 0) mult *= -1;
            return mult;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (input == "")
            {
                result = 0;
                Console.WriteLine("Error");
                return Convert.ToBoolean(result);
            }
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsLetter(input[i]))
                {
                    result = 0;
                    Console.WriteLine("Error");
                    return Convert.ToBoolean(result);
                }
            }
            result = 1;

            try
            {
                if (Convert.ToInt32(input) < 0)
                {
                    Console.WriteLine("Error");
                    result = 0;
                }
            }
            catch
            {
                Console.WriteLine("Error");
                
                TryParseNaturalNumber(input, out result);
            }
           
            return Convert.ToBoolean(result);
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> numbers = new List<int>();
            int value = 0;
            for (int i = 0; i < naturalNumber; i++)
            {
                numbers.Insert(i, value);
                value += 2;
            }

            return numbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (input == "")
            {
                result = 0;
                Console.WriteLine("Error");
                return Convert.ToBoolean(result);
            }
            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsLetter(input[i]))
                {
                    result = 0;
                    Console.WriteLine("Error");
                    return Convert.ToBoolean(result);
                }
            }
            result = 1;

            try
            {
                if (Convert.ToInt32(input) < 0)
                {
                    Console.WriteLine("Error");
                    result = 0;
                }
            }
            catch
            {
                Console.WriteLine("Error");
               
                TryParseNaturalNumber(input, out result);
            }
           
            return Convert.ToBoolean(result);
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string digit = Convert.ToString(digitToRemove);
            string number = Convert.ToString(source);

            for (int i = 0; i < number.Length; i++)
            {
                if (number[i] == digit[0])
                {
                    number = number.Remove(i, 1);
                    i--;
                }
            }

            return number;
        }
    }
}
